# Code Test

Se han definido unos test de aceptación para la creación de una API JSON de usuarios, el objetivo de la prueba es implementar la API y hacer que pasen los tests.

- Debe evitarse editar los tests en la medida de lo posible
- Los usuarios deben persistirse en una base de datos a elección del evaluado
- Se deben gestionar los diferentes errores (en los tests están indicados todos los errores a tener en cuenta)

## Modelo de datos

```ts
export interface User {
  /** Clave primaria. Deber ser un `string` */
  id?: string;

  /** Debe ser único */
  userName: string;

  /** Obligatorio */
  firstName: string;

  /** Obligatorio */
  lastName: string;
}
```

## Scripts

### npm test

Ejecuta todo los tests

### npm start

Inicia la aplicación en el puerto indicado port la variable de entorno `PORT` o en su defecto en el puerto `3000`
