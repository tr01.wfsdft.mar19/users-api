const express   = require('express');
const router    = express.Router();
const MongoClient   = require('mongodb').MongoClient;
const MongoError    = require('mongodb').MongoError;
const ObjectID      = require('mongodb').ObjectID;

const getMongoClient = async()=>{
  return new Promise((resolve, reject)=>{

    MongoClient.connect('mongodb://127.0.0.1:27017/users', { useNewUrlParser: true }, async(error, client)=>{
      if(error){
        reject(new MongoError(error));
      }
      resolve(client);
    });
  });
};

router.get('/api/users', async(request, response)=>{

  /*if(!request.is('application/json')){
    return response.status(400).json({ ok: false, data:[], fails:[{error: 'Request header must be application/json'}] })
  }*/
  const client = await getMongoClient().catch(e=>console.error(e));
  const col = client.db('users').collection('users');
  await col.find({}).toArray((error, docs)=>{
    if(error){
      return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    return response.status(201).json({ ok: true, data: [docs], fails:[] });
  });
  client.close();
});

router.get('/api/users/:id', async(request, response)=>{
  if(!request.params.id){
    return response.status(400).json({ ok: false, data:[], fails:[{error: "User ID required."}] });
  }
  const client = await getMongoClient().catch(e=>console.error(e));
  const col = client.db('users').collection('users');
  await col.findOne({"_id": ObjectID(request.params.id)}, (error, doc)=>{
    if(error){
      return response.status(404).json({ ok: false, data:[], fails:[{error: error}] });
    }
    return response.status(200).json({ ok: true, data: doc, fails:[] });
  });
  client.close();
});

router.post('/api/users', async(request, response)=>{
  /*if(!request.is('application/json')){
    return response.status(400).json({ ok: false, data:[], fails:[{error: 'Request header must be application/json'}] })
  }*/
  let userName = request.body.userName;
  let firstName = request.body.firstName;
  let lastName = request.body.lastName;
  if(!userName || !firstName || !lastName){
    return response.status(400).json({ ok: false, data:[], fails:[{error: "userName, firstName and lastName required"}] });
  }
  const client = await getMongoClient().catch(e=>console.error(e));
  const col = client.db('users').collection('users');
  await col.find({username: userName}).toArray((error, doc)=>{
    if(error){
      return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    if(doc){
      return response.status(409).json({ ok: false, data:[], fails:[{error: "userName already exists"}]});
    }else{
      col.insertOne({username: userName, firstname: firstName, lastname: lastName}, (error, result)=>{
        if(error){
          return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
        }
        return response.status(201).json({ok: true, data: result, fails:[]});
      });
    }
  });
  client.close();
});

router.patch('/api/users/:id', async(request, response)=>{
  /*if(!request.is('application/json')){
    return response.status(400).json({ ok: false, data:[], fails:[{error: 'Request header must be application/json'}] })
  }*/
  if(!request.params.id){
    return response.status(400).json({ ok: false, data:[], fails:[{error: "User ID required."}] });
  }
  let id        = request.params.id;
  let userName  = request.body.userName;
  let firstName = request.body.firstName;
  let lastName  = request.body.lastName;
  if(!userName || !firstName || !lastName){
    return response.status(400).json({ ok: false, data:[], fails:[{error: "userName, firstName and lastName required"}] });
  }
  const client = await getMongoClient().catch(e=>console.error(e));
  const col = client.db('users').collection('users');
  await col.findOneAndUpdate({"_id": ObjectID(id)}, {
    $set: {
      username: userName,
      firstname: firstName,
      lastname: lastName
    }
  }, (error, result)=>{
    if(error){
      return response.status(404).json({ ok: false, data:[], fails:[{error: error}] });
    }
    return response.status(204).json({ ok: true, data:[], fails:[] });
  });
  client.close();
});

router.delete('/api/users/:id', async(request, response)=>{
  if(!request.params.id){
    return response.status(400).json({ ok: false, data:[], fails:[{error: "User ID required."}] });
  }
  const client = await getMongoClient().catch(e=>console.error(e));
  const col = client.db('users').collection('users');
  await col.findOneAndDelete({"_id": ObjectID(request.params.id)}, (error, result)=>{
    if(error){
      return response.status(404).json({ ok: false, data:[], fails:[{error: error}] });
    }
    return response.status(204).json({ ok: true, data:result, fails:[] });
  });
  client.close();
});

module.exports = router;
