export interface User {
  /** Primary key. Must be an UUID */
  id?: string;

  /** Required. Must be unique */
  userName: string;

  /** Required */
  firstName: string;

  /** Required */
  lastName: string;
}
