import express, { Application } from 'express';
import http, { Server } from 'http';
import pEvent from 'p-event';
import util from 'util';

// -----------------------------------
// YOU CAN ONLY EDIT THE FUNCTION BODY
// -----------------------------------

export let server: Server | undefined = undefined;

export function app(): Application {
  const app = express();

  const indexRouter = require('../routes/index');
  app.use(express.json());
  app.use(indexRouter);
  // TODO: must return an express application (without calling `.listen()`) it should configure endpoints and
  //       middlewares before returning it

  return app;
}

export async function tearUp() {
  // it will run before calling start
  // connect to db, initialization, etc.
}

export async function tearDown() {
  // it will run after calling stop
  // close db connections, etc.
}

// -----------------------------------------------------------------------------
// DO NOT EDIT THE CODE BELOW
// -----------------------------------------------------------------------------

export async function start({ port = 3000 } = {}) {
  const application = app();

  server = http.createServer(application).listen(port);

  await pEvent(server, 'listening');

  return application;
}

export async function stop() {
  /* istanbul ignore next */
  if (!server) {
    return;
  }

  await util.promisify(server.close).call(server);
  server = undefined;
}
