import { start, tearDown, tearUp } from './app';

const port = process.env.PORT ? Number(process.env.PORT) : 3000;

async function main() {
  try {
    await tearUp();
    await start({ port });
  } catch (err) {
    throw err;
  }
}

if (require.main === module) {
  main()
    .then(() => {
      console.log(`app listening on http://localhost:${port}`);
    })
    .catch(err => {
      console.log('the app crashed');
      console.log(err.message);
      console.log(err.stack);

      return tearDown().finally(() => process.exit(1));
    });
}
