import faker from 'faker';
import supertest, { SuperTest, Test } from 'supertest';
import { start, stop, tearDown, tearUp } from '../src/app';
import { User } from '../src/user';
import { Application } from 'express';
import { expect } from 'chai';

describe('Users', () => {
  let app: Application;
  let client: SuperTest<Test>;

  before(tearUp);
  before(async () => {
    app = await start();
    client = supertest(app);
  });

  after(tearDown);
  after(stop);

  context('POST /users', () => {
    it('should return 201 - Created when an user is created', async () => {
      const user = createUser();

      const response = await client
        .post('/api/users')
        .send(user)
        .expect(201)
        .expect('content-type', /application\/json/);

      expect(response.body).to.have.property('id');
      expect(response).to.containSubset(user);
    });

    it('should should return 400 - Bad Request when id is set', async () => {
      const user = createUser();
      user.id = faker.random.uuid();

      await client.post('/api/users').send(user);
      await client
        .post('/api/users')
        .send(user)
        .expect(400)
        .expect('content-type', /application\/json/);
    });

    it('should should return 400 - Bad Request when userName is not set', async () => {
      const user = createUser();
      delete user.userName;

      await client.post('/api/users').send(user);
      await client
        .post('/api/users')
        .send(user)
        .expect(400)
        .expect('content-type', /application\/json/);
    });

    it('should should return 400 - Bad Request when firstName is not set', async () => {
      const user = createUser();
      delete user.firstName;

      await client.post('/api/users').send(user);
      await client
        .post('/api/users')
        .send(user)
        .expect(400)
        .expect('content-type', /application\/json/);
    });

    it('should should return 400 - Bad Request when lastName is not set', async () => {
      const user = createUser();
      delete user.lastName;

      await client.post('/api/users').send(user);
      await client
        .post('/api/users')
        .send(user)
        .expect(400)
        .expect('content-type', /application\/json/);
    });

    it('should return 409 - Conflict when the userName already exists', async () => {
      const user = createUser();

      await client.post('/api/users').send(user);
      await client
        .post('/api/users')
        .send(user)
        .expect(409)
        .expect('content-type', /application\/json/);
    });
  });

  context('GET /users', () => {
    it('should return 200 - Ok when the user are listed', async () => {
      await client.post('/api/users').send(createUser());

      const response = await client
        .get('/api/users')
        .expect(200)
        .expect('content-type', /application\/json/);

      const users = response.body as User[];

      expect(users)
        .to.be.an('array')
        .with.length.of.at.least(1);

      users.slice(10).forEach((user, index) => {
        const message = `the user at ${index}`;
        expect(user, message).to.have.property('id');
        expect(user, message).to.have.property('userName');
        expect(user, message).to.have.property('firstName');
        expect(user, message).to.have.property('lastName');
      });
    });
  });

  context('GET /users/:id', () => {
    it('should return 200 - Ok when a single user is obtained', async () => {
      const { body: user } = await client.post('/api/users').send(createUser());

      const response = await client
        .get(`/api/users/${user.id}`)
        .expect(200)
        .expect('content-type', /application\/json/);

      expect(response.body).to.containSubset(user);
    });

    it('should return 404 - Not Found when the user does not exists', async () => {
      await client
        .get(`/api/users/${faker.random.uuid()}`)
        .expect(404)
        .expect('content-type', /application\/json/);
    });
  });

  context('DELETE /users/:id', () => {
    it('should return 204 - No Content when an user is deleted', async () => {
      const { body: user } = await client.post('/api/users').send(createUser());

      await client.delete(`/api/users/${user.id}`).expect(204);

      await client
        .get(`/api/users/${user.id}`)
        .expect(404)
        .expect('content-type', /application\/json/);
    });

    it('should return 404 - Not Found when the user does not exists', async () => {
      await client
        .delete(`/api/users/${faker.random.uuid()}`)
        .expect(404)
        .expect('content-type', /application\/json/);
    });
  });

  context('PATCH /users/:id', () => {
    it('should return 204 - No Content when an user firstName is updated', async () => {
      const { body: user } = await client.post('/api/users').send(createUser());
      const update: Partial<User> = { firstName: faker.name.firstName() };

      await client
        .patch(`/api/users/${user.id}`)
        .send(update)
        .expect(204);

      const response = await client.get(`/api/users/${user.id}`);

      expect(response.body.lastName).to.be.containSubset(update);
    });

    it('should return 204 - No Content when an user lastName is updated', async () => {
      const { body: user } = await client.post('/api/users').send(createUser());
      const update: Partial<User> = { lastName: faker.name.lastName() };

      await client
        .patch(`/api/users/${user.id}`)
        .send(update)
        .expect(204);

      const response = await client.get(`/api/users/${user.id}`);

      expect(response.body.lastName).to.be.containSubset(update);
    });

    it('should return 204 - No Content when an user userName is updated', async () => {
      const { body: user } = await client.post('/api/users').send(createUser());
      const update: Partial<User> = { userName: createUser().userName };

      await client
        .patch(`/api/users/${user.id}`)
        .send(update)
        .expect(204);

      const response = await client.get(`/api/users/${user.id}`);

      expect(response.body.lastName).to.be.containSubset(update);
    });

    it('should return 404 - Not Found when the user does not exists', async () => {
      await client
        .patch(`/api/users/${faker.random.uuid()}`)
        .expect(404)
        .expect('content-type', /application\/json/);
    });

    it('should return 409 - Conflict when an user userName already exists', async () => {
      const { body: user } = await client.post('/api/users').send(createUser());
      const { body: anotherUser } = await client.post('/api/users').send(createUser());

      await client
        .patch(`/api/users/${user.id}`)
        .send({ userName: anotherUser.userName })
        .expect(409)
        .expect('content-type', /application\/json/);
    });
  });
});

function createUser(user: Partial<User> = {}): User {
  return {
    userName: [faker.internet.userName(), faker.random.uuid()].join('_'),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    ...user
  };
}
